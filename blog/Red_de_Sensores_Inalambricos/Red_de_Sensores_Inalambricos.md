Title: De la Ficción a la Realidad, Red de Sensores Inalámbricos	
Date: 2016-09-02 00:00
Slug: De-la-Ficcion-a-la-Realidad-Red-de-Sensores-Inalambricos
Tags: eventos, charlas, iot, domótica
Author: LEXO
Category: Blog
Summary: Evento de charla sobre el uso del ESP8266 como sistema meteorológico y más.

**¿Habías escuchado del ESP8266?** Se trata de un **SoC WiFi** de precio muy accesible que **se puede programar** en muchos lenguajes, como **C, Lua, Python, Arduino**, entre otros y es utilizado por muchos **para hacer IoT**.

**Imagina** una implementación donde se realice **monitoreo de variables ambientales, haciendo uso de MQTT** como mecanismo de transporte ademas de **Docker, Elastic Search y Grafana**. Hasta aquí casi tenemos la receta completa, pero aún nos falta romper una pequeña limitante «**reconfigurar dinámicamente el ESP8266** sin la necesidad de tener que reprogramarlo», esto nos permitirá ajustar el comportamiento de cada nodo en la red sin necesidad de tener acceso físico a el.

**De esta implementación es que nos viene a hablar Jesús Carrillo al HackLab, compartiéndonos su experiencia y ayudándonos a entender cómo podríamos hacerlo nosotros mismos.**

Nos vemos en el HackLab el día lunes 12 de Septiembre a las 7:30 PM 

![Flyer]({attach}Flyer.jpg)
