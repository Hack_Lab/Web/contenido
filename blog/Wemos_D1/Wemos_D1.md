Title: WeMos D1 - IoT Al Estilo Arduino
Date: 2016-01-13
Tags: iot, arduino, esp8266, wifi, equipamiento
Slug: wemos-d1
Author: LEXO
Category: Blog
Summary: Nueva herramienta para IoT

El día de hoy hemos hecho una nueva adquisición para el laboratorio. Se trata de una **WeMos D1** que no es otra cosa que una tarjeta en formato de un Arduino UNO (compatible), solo que en sustitución del ATmega328P le han montado un **ESP8266**.

Así que si tienes interés en aprender, experimentar o algún proyecto de IoT ven a explotar tu imaginación.

---

**Características:**

**CPU (SoC):** ESP8266 (32Bit)
**Velocidad de CPU:** 80(160)MHz
**RAM:** 64KB
**Flash:** 4MB
**USB:** CH340 USB-UART
**WiFi:** 2.4 GHz 802.11n
**Puertos de I/S:** 11 Digitales y 1 Análogo 

![Wemos D1]({attach}Wemos_D1.jpg)
