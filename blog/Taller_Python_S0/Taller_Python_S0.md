Title: Taller Colaborativo de Programación con Python	
Date: 2018-03-06 00:00
Slug: Taller-Colaborativo-de-Programacion-con-Python
Tags: eventos, taller, python, colaboración
Author: AngelMalvada
Category: Blog
Summary: Taller Colaborativo de Programación con Python (Introducción S0)

El próximo **sábado 10 de marzo de 2018** comenzaremos lo que prendendemos sea el arranque de nuestro primer taller colaborativo, con una dinámica que se centre en técnicas de aprendizaje colectivas y cooperativas, más allá del hágalo usted mismx (hagámoslo juntxs).

La primer sesión arranca con la introducción, enfocándose a la banda que inicia desde cero y de acuerdo a los avances; de manera colectiva/propositiva nos vamos a ir definiendo temas objeto de aprendizaje para las siguientes sesiones.
![Flyer]({attach}Flyer.jpg)
