Title: Hackmitin 2016
Date: 2016-11-28 00:00
Slug: hackmitin-colima
Tags: eventos, hackmitin, cultura libre
Author: AngelMalvada
Category: Blog
Summary: Este cierre de año pinta bastante bien, pues Colima será sede del Hackmitin 2016.

¿Y eso con qué se come?

**Hackmitin** es un **encuentro anual** de personas que trabajan la tecnología desde una **visión libre y comunitaria**. Un fin de semana abierto al público con charlas y talleres gratuitos, dedicado a las tecnologías libres y a experimentar modelos alternativos de trabajo y cooperación.

El encuentro está destinado a todo tipo de personas que tengan una mente abierta y curiosa, con ganas de compartir sus experiencias y participar. Algunas charlas y talleres pueden requerir conocimientos informáticos avanzados pero no todos.

El **Hackmitin** se ha realizado desde el 2009 en distintas partes del país y desde diferentes espacios autogestivos. En todos los encuentros se han generado experiencias únicas de aprendizaje y colaboración.

Este año **Colima Hacklab** será el anfitrión del encuentro, que se llevará a cabo del **9 al 11 de diciembre**. La organización del evento se da a través de plataformas colaborativas, donde todas las personas interesadas pueden participar desde distintas partes del país.

![HackMitin 2016]({attach}hackmitin_large.png)

¿Te interesa participar? Comparte tus inquietudes o propón un taller: [espora.org](http://hackmitin.espora.org)

También puedes echarle un vistazo al spot desde acá:
[archive.org](https://archive.org/details/hackmitin_2016)
