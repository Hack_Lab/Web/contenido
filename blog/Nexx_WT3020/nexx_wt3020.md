Title: Router Nexx WT3020
Date: 2016-01-04
Tags: hackeable, equipamiento
Slug: router-nexx-wt3020
Author: LEXO
Category: Blog
Summary: Nueva herramienta para hackear


Iniciamos bien el año nuevo con la llegada de un nuevo equipo (hackeable) el cual se suma al acerbo de nuestro espacio.

Se trata de un Router **Nexx WT3020F** el cual inicialmente podemos utilizar como punto de acceso WiFi, repetidor e incluso implementar una muy pequeña NAS.

---

**Características:**

**CPU (SoC):** MediaTek MT7620N
**Velocidad de CPU:** 580MHz
**RAM:** 64 MB
**Flash:** 8MB
**Ethernet 10/100 Mbps:** 2
**USB 2.0:** 1
**WiFi:** 2.4 GHz 802.11n MiMo 2x2:2
**Dimensiones:** 6.5 x 4.5 x 2.0 cm

![Outside]({attach}Nexx_WT3020_1.jpg)
![Inside]({attach}Nexx_WT3020_2.jpg)
