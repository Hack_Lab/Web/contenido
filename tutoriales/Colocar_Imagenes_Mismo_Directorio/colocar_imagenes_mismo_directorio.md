Title: Colocar imágenes en el mismo directorio de el contenido
Date: 2019-10-31 13:00
Slug: pelican-imagenes-mismo-directorio-contenido
Tags: Pelican, Blog, Web
Author: Batuto
Category: Tutoriales
Summary: Uso de contenido estático en el mismo directorio de una nota.
Status: published

La etiqueta de categoría  debe coincidir con la categoría que se desea. p.e.
Category: Tutoriales

Se pone un directorio para el archivo de contenido de la siguiente forma
<br>
` ` `
content 
` ` `
<br>
` ` `
├── images 
` ` `
<br>
` ` `
│   └── any.jpg
` ` `
<br>
` ` `
├── pdfs
` ` `
<br>
` ` `
│   └── any.pdf
` ` `
<br>
` ` `
└── pages
` ` `
<br>
` ` `
└── Tuturials
` ` `
<br>
` ` `
│   └── test imagenes
` ` `
<br>
` ` `
│     └── como_poner_imagenes.md
` ` `
<br>
` ` `
│     └──  test.jpg 
` ` `

Y en caso de ser MarkDown en el archivo veríamos `![Photo by Marius Schmidt from Pexels]({attach}test.jpg) `

![Photo by Marius Schmidt from Pexels]({attach}test.jpg)
