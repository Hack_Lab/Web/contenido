Title: Llavero GnuPG USB de Software Libre con Gnuk
Date: 2020-05-15 20:21
Tags: GnuPG, Gnuk, OpenPGP
Authors: LEXO
Category: Tutoriales
Summary: Llavero GnuPG USB de Software Libre con Gnuk
Status: published

Para quienes hemos decidido agregar una capa extra de cifrado y seguridad a nuestras comunicaciones con GnuPG (la implementación de software libre en GNU/Linux de OpenPGP), es de sumo cuidado tener mucho cuidado en dónde y como almacenamos nuestras llaves GPG, así que hay quienes optamos por guardarlas en nuestro disco duro o USB cifrada, pero en este pequeño tutorial vamos a experimentar con Gnuk, una alternativa que promete un poco más de protección.

# ¿Qué es Gnuk?
______

Firmware de software libre que -mágicamente- corre en chips STM32F103 para armar dispositivos USB de criptografía, como es el caso de la implementación del protocolo OpenPGP compatible con nuestro gran aliado GnuPG.

# Pros y contras
________________

**Pros**

1. Difícilmente se puede vulnerar con un ataque de fuerza bruta.
2. Mejor protección contra intentos de clonaciónen en comparación con un disco duro o USB (aún estando cifrada).

**Contras**

1. Dependencia a un dispositivo más

# ¿Qué se necesita?
____________________

**Hardware**

1. Programador ST Link
2. Maple Mini
3. Cables 
4. Cautin
5. Soldadura

**Software**

1. GNU/Linux (para este tutorial de uso Devual Beowulf)
2. GnuPG
3. OpenOCD

# Manos a la obra 
_________________

**Paquetes y dependencias necesarias**

~~~
apt install git build-essential gcc-arm-none-eabi scdaemon
~~~

**A descargar fuentes y compilar**
~~~
git clone git://git.gniibe.org/gnuk/gnuk.git/
cd gnuk/
git submodule update --init
./configure --vidpid=234b:0000 --target=MAPLE_MINI --enable-factory-reset
make
~~~

**Configuración de hardware**


| Maple Mini &nbsp; &nbsp; &nbsp; &nbsp; | ST Link Programmer |
|------------|--------------------|
| 21         | CLK                |
| 22         | IO                 |
| GND        | GND                |


![Programando Maple Mini]({attach}st_link_maple_mini.jpg)

**Grabar firmware**

Una vez que conectamos los pines de la Maple Mini con el programador ST Link, el procedimiento de escritura del firmware lo haremos con OpenOCD de la siguiente forma:

1. Conectar el programador ST Link por USB a la PC
2. Conectar la Maple Mini por USB a la PC
3. Ejecutar (con privilegios de root) el comando de escritura de firmware que aparece acá abajo (adecuando la ubicación del archivo gnuk.bin que se genera al compilar en el directorio build)

~~~
openocd -f /usr/share/openocd/scripts/interface/stlink-v2.cfg \
-f /usr/share/openocd/scripts/target/stm32f1x.cfg -c "init" \
-c "reset" -c "reset halt" -c "stm32f1x unlock 0" -c "reset halt" \
-c "stm32f1x mass_erase 0" -c "flash write_bank 0 build/gnuk.bin 0" \
-c "stm32f1x lock 0" -c "reset halt" -c "shutdown"
~~~

![OpenOCD Programando]({attach}openocd.gif)

# Ahora si, a probar
____________________

Ahora que ha terminado OpenOCD de hacer su chamba, podemos desconectar todo, conectar únicamente por USB la Maple Mini a la PC y corroborar que ya tenemos listo nuestro llavero con le siguiente comando:

~~~
gpg --card-status
~~~

![Card status]({attach}card_status.gif)
