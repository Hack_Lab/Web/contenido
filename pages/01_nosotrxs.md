Title: Nosotrxs
Date: 2010-12-03 10:20
Modified: 2010-12-03 10:20
Authors: Alexis Metaireau, Conan Doyle
Summary: Short version for index and feeds

---
# "Laboratorio autónomo de experimentación y producción colaborativa"

---

Es un espacio autónomo, libre y abierto al público que provee de la infraestructura física y virtual necesaria para que personas con intereses en el conocimiento, ciencia, tecnología, arte digital y electrónica, realicen actividades de investigación, experimentación e innovación en un entorno cooperativo y colaborativo.


* Promovemos la libertad y la autonomía.
* Apoyamos iniciativas que detonen el autoaprendizaje, conocimiento experimental, compartir, la solidaridad y la cooperación.
* Impulsamos la cultura hacker en el sentido del MIT (Massachusetts Institute of Technology)
* Practicamos la cultura hazlo tu mismo (DIY)
