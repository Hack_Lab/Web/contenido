Title: Apóyanos
Template: page

---
# ¡Ayúdanos a resistir!
---

Durante años el HackLab ha resistido como el único espació en su clase en esta ciudad y hasta el día de hoy es el único lugar que representa la cultura de un hackerspace.

El HackLab es autogestivo, no recibe apoyo alguno de instituciones públicas del estado o partido político y es así como desde esa autonomía te invitamos a colaborar y llevarte algo con lo que acá experimentamos, armamos, configuramos y/o construimos.

Nota: Si eres de Colima o crees que hay otro modo para envíos no dudes en contactarnos, mientras tanto nos disculpamos por el uso de la plataforma de mercado libre (así podemos reducir un poco los costos de envío y garantizar el rembolso para ambas partes en caso de "extravíos" por parte de la paquetearía).

# [Thinkpad X230 Con Coreboot + Heads]
> [$ 8,150.00 MXN]{: style="color: blue"} <br><br>
> ¿Necesitas una laptop con una BIOS que procure seguridad como [NitroPad] / [Insurgo PrivacyBeast X230] sin batallar por conseguir el hardware correcto o dañarlo al instalar [Coreboot+Heads] y al mismo tiempo ayudarnos a solventar gastos del HackLab? Acá te ofrecemos una alternativa más accesible y con las siguientes características:<br>

* Lenovo Thinkpad X230
* Intel i5-3320M CPU @ 2.60GHz (3gen)
* RAM 8GB
* SSD 120GB
* WiFi 2×2 AGN (Atheros compatible con GNU/Linux-libre) 
* Llavero USB con Gnuk o Generador USB de números aleatorios con NeuG
* 12 meses de garantía por fallas de hardware

[Thinkpad X230 Con Coreboot + Heads]: https://articulo.mercadolibre.com.mx/MLM-847374340-thinkpad-x230-con-coreboot-heads-_JM 
[$ 8,150.00 MXN]: https://articulo.mercadolibre.com.mx/MLM-847374340-thinkpad-x230-con-coreboot-heads-_JM
[NitroPad]: https://shop.nitrokey.com/shop/product/nitropad-x230-67
[Insurgo PrivacyBeast X230]: https://insurgo.ca/produit/qubesos-certified-privacybeast_x230-reasonably-secured-laptop/
[Coreboot+Heads]: https://trmm.net/Heads/
![X230 Heads](https://live.staticflickr.com/5574/30450989320_f6504cb662_b.jpg)

# [Llavero GnuPG USB de Software Libre]
> [$ 680.00 MXN]{: style="color: blue"} <br><br>
Se trata de un pequeño dispositivo que hemos armado en el HackLab en el que podrás almacenar llaves GnuPG con un mayor grado de confiabilidad que almacenar tu llave privada en una memoria USB.
[Llavero GnuPG USB de Software Libre]: https://articulo.mercadolibre.com.mx/MLM-847227573-llavero-gnupg-usb-de-software-libre-_JM
[$ 680.00 MXN]: https://articulo.mercadolibre.com.mx/MLM-847227573-llavero-gnupg-usb-de-software-libre-_JM
![Llavero GnuPG USB](https://http2.mlstatic.com/D_NQ_NP_2X_682314-MLM44253379095_122020-F.webp)
